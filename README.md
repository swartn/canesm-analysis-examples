# CanESM analysis examples

Some examples of basic analysis of CanESM.

Demonstrates various methods of downloading and visualizing CanESM data

Can use on binder:

https://binder.pangeo.io/v2/gl/swartn%2Fcanesm-analysis-examples/master?filepath=canesm5_fgco2_example_plots.ipynb

Neil Swart, 2020

